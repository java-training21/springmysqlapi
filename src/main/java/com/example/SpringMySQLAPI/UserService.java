package com.example.SpringMySQLAPI;
//UserService is a class for DB connection. Thanks to the UserRepo (CrudRepository) we can use basic methods like a findAll,save,findById, deleteById

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
//@Transactional
public class UserService {
    @Autowired
    private UserRepo userRepo;

    public List<Users> listAllUser() {
        return (List<Users>) userRepo.findAll();
    }

    public void saveUser(Users users) {
        userRepo.save(users);
    }

    public Users getUser(Integer id) {
        return userRepo.findById(id).get();
    }

    public void deleteUser(Integer id) {
        userRepo.deleteById(id);
    }
}

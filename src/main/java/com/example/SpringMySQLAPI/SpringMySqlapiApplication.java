package com.example.SpringMySQLAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMySqlapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMySqlapiApplication.class, args);
	}

}

package com.example.SpringMySQLAPI;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//IDENTITY inkrementacja o 1, AUTO-doesn't work. With the generation GenerationType.AUTO hibernate will look for the default hibernate_sequence table , so change generation to IDENTITY
    private Integer id;

    private String login;
    private String password;
    //private String message;
    private int value;
    //private LocalDateTime lastLogindate;

    public Users(String login, String password, int value) {
        this.login = login;
        this.password = password;
        //this.message = message;
        this.value = value;
    }

    //wykomentowane
    public String toString() {
        return String.format("User[id=%d,login='%s', password='%s', value=%d]", id, login, password, value);
    }


    public Users() { //bezparametrowy konstruktor  //wiyhout this POST method works fine but GET no
    }


    public Integer getId(){
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}


package com.example.SpringMySQLAPI;
//UserController is a class for make the API, which uses the data from the DB. Is connected with the UserController.

//import com.example.SpringMySQLAPI.model.User;
//import com.example.SpringMySQLAPI.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("")
    public List<Users> list() {
        return userService.listAllUser();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Users> get(@PathVariable Integer id) {
        try {
            Users users = userService.getUser(id);
            return new ResponseEntity<Users>(users, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Users>(HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/")
    public void add(@RequestBody Users users) {
        userService.saveUser(users);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody Users users, @PathVariable Integer id) {
        try {
            Users existUser = userService.getUser(id);
            users.setId(id);
            userService.saveUser(users);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {

        userService.deleteUser(id);
    }
}
